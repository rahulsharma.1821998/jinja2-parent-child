from django.shortcuts import render

# Create your views here.


def parent(request):
    return render(request, "parent.html")


def child(request):
    return render(request, "child.html")


def grandchild(request):
    return render(request, "grandchild.html")

#start nested extends 


def base(request):
    return render(request, "base.html")


def layout(request):
    return render(request, "layout.html")


def home(request):
    return render(request, "home.html")

#required block 
def page(request):
    return render(request, "page.html")


def issue(request):
    return render(request, "issue.html")


def bug_report(request):
    return render(request, "bug_report.html")


#super keyword example
def superParent(request):
    return render(request, "superkeywordexmaple/parent.html")


def superChild(request):
    return render(request, "superkeywordexmaple/child.html")

