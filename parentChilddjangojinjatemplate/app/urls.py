from django.urls import path,include
from .views import *

urlpatterns = [
    path('', parent),
    path('child/',child),
    path('grandchild/', grandchild),
    path('base/',base),
    path('layout/', layout),
    path('home/', home),
    path('page/', page),
    path('issue/', issue),
    path('bug_report/', bug_report),
    path('superParent/', superParent),
    path('superChild/', superChild)
    
    
]
